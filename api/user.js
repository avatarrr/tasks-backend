const bcrypt = require('bcrypt-nodejs');

module.exports = app => {
  const obtainHash = (password, callback) => {
    bcrypt.genSalt(10, (error, salt) => {
      bcrypt.hash(password, salt, null, (error, hash) => callback(hash));
    })
  };

  const save = (request, response) => {
    obtainHash(request.body.password, hash => {
      const password = hash;

      app.db('users').insert({ name: request.body.name, email: request.body.email, password }).then(_ => response.status(204).send()).catch(error => response.status(400).json(error));
    })
  }

  return { save }
};