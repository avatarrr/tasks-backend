const consign = require('consign');
const express = require('express');
const db = require('./config/db');

const app = express();

consign().then('./config/middlewares.js').then('./api').then('./config/routes.js').into(app);

app.db = db;

app.listen(3000);